const express = require('express')
const router = express.Router()
const Products = require('../model/productInfo')

//Create
router.post('/', async(req,res) => {
    const productInfo = new Products({
        uuid: req.body.uuid,
        minor: req.body.minor,
        major: req.body.major,
        website: req.body.website,
        pic_url: req.body.pic_url
    })

    try{
        const savedCommend = await productInfo.save()
        res.json(savedCommend)
    }catch(err){
        res.json(err)
    }
})

//Update
router.patch('/:postId', async (req,res) => {
    try{
    const productInfo = await Products.updateOne(
        {_id: req.params.postId}, 
        {$set: {website: req.body.website,pic_url: req.body.pic_url}}
     );
    res.json(productInfo);    
    }catch (err) {
    res.json(err);
    }
})

//Delete
router.delete('/:postId', async (req,res) => {
    try {
    const removedCommend = await Products.remove({_id: req.params.postId}) 
    res.json(removedCommend);
    }catch (err) {
    res.json(err);
    }
});

//getAll
router.get('/', async(req, res) => {
    try{
        const comment = await Products.find();
        res.json(comment);
    }catch(err){
        res.json(err);
    }
});

//get target doc
router.get('/:uuid/:minor/:major', async(req, res) => {
    try{
        const comment = await Products.find({"uuid": req.params.uuid, "minor": req.params.minor, "major": req.params.major});
         res.json(comment);
    }catch(err){
        res.json(err);
    }
});

// All the commends of commendID
router.get('/:postId', async (req,res) => {
    try {
         const comment = await Products.find({"_id": req.params.postId});
         res.json(comment);
    }catch (err) {
        res.json(err);
    }    
});

module.exports = router;