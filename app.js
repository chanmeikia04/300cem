const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv/config');

app.use(bodyParser.json());
app.use(cors());


//Import Routes
const productsRoute = require('./routes/product');

app.use('/products', productsRoute);


//Routes
app.get('/', (req,res) => {res.send('testing server');});


//Connect to DB
mongoose.connect( process.env.DB_CONNECTION, { useNewUrlParser: true },  () => console.log('connected to DB!'));


//start Listening
app.listen(3000);