const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
    uuid: { 
        type: String,
        require: true
    },
    minor:{ 
        type: String,
        require: true   
    },
    major:{ 
        type: String,
        require: true   
    },
    website:{ 
        type: String,
        require: true   
    },
    pic_url:{ 
        type: String,
        require: true   
    }
});


module.exports = mongoose.model('Products', ProductSchema);